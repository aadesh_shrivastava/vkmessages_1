package utils.asynctaskloader;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

public final class NetTask extends AsyncTask<Void, String, String> {
    
    protected final Resources mResources;
    
    private String mResult;
    private String mUrl;
    private String mError;
    private int mType;
    private String mProgressMessage;
    private IProgressTracker mProgressTracker;

    /* UI Thread */
    public NetTask(Resources resources, String url, int type) {
		// Keep reference to resources
		mResources = resources;
		mUrl = url;
		mType = type;
		mError = "";
		// Initialise initial pre-execute message
		//mProgressMessage = resources.getString(R.string.task_starting);
    }
    
    public String getUrl() {
    	return mUrl;
    }
    
    public String getError() {
    	return mError;
    }
    
    public int getType() {
    	return mType;
    }
    
    /* UI Thread */
    public void setProgressTracker(IProgressTracker progressTracker) {
		// Attach to progress tracker
		mProgressTracker = progressTracker;
		// Initialise progress tracker with current task state
		if (mProgressTracker != null) {
		    mProgressTracker.onProgress(mProgressMessage);
		    if (mResult != null) {
		    	mProgressTracker.onComplete();
		    }
		}
    }

    /* UI Thread */
    @Override
    protected void onCancelled() {
    	// Detach from progress tracker
    	mProgressTracker = null;
    }
    
    /* UI Thread */
    @Override
    protected void onProgressUpdate(String... values) {
		// Update progress message 
		mProgressMessage = values[0];
		// And send it to progress tracker
		if (mProgressTracker != null) {
		    mProgressTracker.onProgress(mProgressMessage);
		}
    }

    /* UI Thread */
    @Override
    protected void onPostExecute(String result) {
		// Update result
		mResult = result;
		// And send it to progress tracker
		if (mProgressTracker != null) {
		    mProgressTracker.onComplete();
		}
		// Detach from progress tracker
		mProgressTracker = null;
    }

    /* Separate Thread */
    @Override
    protected String doInBackground(Void... arg0) {
		// Working in separate thread
    	HttpURLConnection urlConnection = null;    	
    	StringBuilder builder =  new StringBuilder();
    	int TIMEOUT_VALUE = 20000;

	    try {
	    	URL url = new URL(mUrl);
	    	Log.d("NetTask", mUrl);
		    urlConnection = (HttpURLConnection) url.openConnection();
		    urlConnection.setFollowRedirects(true);
		    //urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
		    urlConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
		    urlConnection.setRequestMethod("GET");
		    urlConnection.setConnectTimeout(TIMEOUT_VALUE);
		    urlConnection.setReadTimeout(TIMEOUT_VALUE);

		    urlConnection.connect();
		    int responseCode = urlConnection.getResponseCode();
		    if (responseCode == HttpURLConnection.HTTP_OK) {		    	
		    	BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
		    	String inputLine;
		        while ((inputLine = in.readLine()) != null) {
		        	builder.append(inputLine);
		        	builder.append("\n");
		        	if (isCancelled()) {
			        	   // This return causes onPostExecute call on UI thread
		        		//mError = "Error: "+mResources.getString(R.string.task_cancelled);
			        	return null;
			   		}
		        }
		        in.close();
		    }
		    else {
		    	//mError = "Error: "+mResources.getString(R.string.task_noconnection);
		    	return null;
		    }
	    }
	    catch (Exception e) {
			mError = "Error:"+e.getMessage();
			return null;
		}
	    finally {
	    	if (urlConnection != null)
	    		urlConnection.disconnect();
	    }
		// This return causes onPostExecute call on UI thread
		return builder.toString();
    }  
}