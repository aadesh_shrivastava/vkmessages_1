package ikillingtime.com.ivkontaktepro;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import org.json.JSONObject;

import java.util.ArrayList;

public class AdpVideos extends BaseAdapter {
    
	private static final String TAG = "AdpVideos";
    private Activity activity;
    private ArrayList<ClsVideos> data;
    private LayoutInflater inflater=null;
    private int currentMode;
    protected AQuery listAq;

    public AdpVideos(Activity a, ArrayList<ClsVideos> d,int mode) {
        activity = a;
        data = d;
        currentMode = mode;
        if (activity==null) return;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listAq = new AQuery(activity);
    }

    public int getCount() {
        return data.size();
    }
    
    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
    	
        return position;
    }
    
    public static class ViewHolder{
        public TextView header,body,time,vidtype;
        public ImageView img,more;
        public ImageView online,multiChat;
    }
    


    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        final ViewHolder holder;
        
        if(convertView==null){
        	vi = inflater.inflate(R.layout.videos_row, null);
            holder=new ViewHolder();
            holder.header=(TextView)vi.findViewById(R.id.dlg_header);
            holder.body=(TextView)vi.findViewById(R.id.dlg_body);
            holder.time=(TextView)vi.findViewById(R.id.dlg_time);
            holder.vidtype=(TextView)vi.findViewById(R.id.vid_type);
            holder.img = (ImageView)vi.findViewById(R.id.user_thumb);
            holder.more = (ImageView)vi.findViewById(R.id.button_more);

            vi.setTag(holder);
        }
        else
            holder=(ViewHolder) vi.getTag();

        AQuery aq = listAq.recycle(convertView);
        final ClsVideos o = data.get(position);
        if (o != null) {
            String color="";
            if (o.isViewved()) {
                holder.header.setTextColor(Color.DKGRAY);
            }
            else{
                holder.header.setTextColor(Color.WHITE);
            }
            aq.id(holder.img).image(o.getThumb(), true, false, 0, 0, null, AQuery.FADE_IN_NETWORK, 9.0f/16.0f);
        	holder.header.setText(Html.fromHtml(o.getTitle()+" "+o.getDescription()));
            holder.more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showPopup(holder.more,o,holder.header.getWidth());
                }
            });
        	holder.time.setText(o.getDuration());
            holder.vidtype.setText(o.getVidtype());

        }
        return vi;
    }

    private void showPopup(View anchorView,final ClsVideos o,int width) {

        String[] stringArray = new String[] { activity.getString(R.string.vid_actlist1), activity.getString(R.string.vid_actlist2),
                !AndroidApplication.getPrefsString("CURVIDGID").equals("")?activity.getString(R.string.vid_actlist4):activity.getString(R.string.vid_actlist5) };

        final android.support.v7.internal.widget.ListPopupWindow popup = new android.support.v7.internal.widget.ListPopupWindow(activity);
        popup.setAdapter(new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1,stringArray ));
        popup.setAnchorView(anchorView);
        popup.setModal(true);
        popup.setWidth(Math.max(300,width));
        popup.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                popup.dismiss();

                if (position<=1) {
                    String s = position==0?"video/*":"*/*";
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        Uri data = Uri.parse(o.getHref());
                        intent.setDataAndType(data, s);
                        activity.startActivity(intent);

                    }
                    catch (Exception e) {
                        Toast.makeText(activity, e.getMessage(), Toast.LENGTH_LONG).show();
                    };
                }
                else if (position==2) {
                    Bundle params = new Bundle();
                    try {
                        String[] s = o.getVid().split("_");
                        params.putString("oid", s[0]);
                        params.putString("vid", s[1]);

                    }
                    catch (Exception e) {
                        Log.e(TAG,e.toString());
                    }

                    listAq.ajax(ActNewsOld.generateUrl(params, (!AndroidApplication.getPrefsString("CURVIDGID").equals("")) ? "video.add" : "video.delete"), JSONObject.class,
                            1, AdpVideos.this, "onAction");

                }
            }
        });
        popup.show();
    }

    public void onAction(String url, JSONObject json, AjaxStatus status) {
        if (json!=null && !json.toString().contains("error_msg")) {
            ActNewsOld.alert(activity, "OK");
        }
        else {
            ActNewsOld.alert(activity, json == null ? "error" : json.toString());
        }
    }
}

