package ikillingtime.com.ivkontaktepro;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.serviceloader.RESTService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ActSearch extends Fragment {

	private static final String TAG = "ActSearch";
	private Activity activity;
	private ArrayList<ClsProfile> profiles,savedProfiles;
	private AdpContacts adapter;
	private ListView list;
	private ResultReceiver mReceiver;
	private String metod;
	private int currentMode=0;
	private String sig;
	private ImageView progressBar;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        metod = "/method/execute?access_token="+AndroidApplication.getPrefsString("access_token");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.search, container, false);
        final EditText schTxt = (EditText)v.findViewById(R.id.sch_searchtxt);
        list = (ListView) v.findViewById(R.id.list);
        progressBar = (ImageView) v.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        
        
        mReceiver = new ResultReceiver(new Handler()) {

            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultData != null && resultData.containsKey("REST_RESULT")) {
                    onRESTResult(resultCode, resultData.getString("REST_RESULT"),resultData.getInt("EXTRA_MODE", 0));
                }
                else {
                    onRESTResult(resultCode, null, 0);
                }
                
            }
            
        };
        
        schTxt.setOnKeyListener(onSchTxt);
        
       
        schTxt.setText("");
        //TODO ?? apply filters on fly or reset?
        schTxt.addTextChangedListener(new TextWatcher(){
    	    public void afterTextChanged(Editable s){
    	        filterByName(s.toString());
    	    }
    	    public void beforeTextChanged(CharSequence s, int start, int count, int after){
    	    	saveProfiles(s.toString());
    	    }
    	    public void onTextChanged(CharSequence s, int start, int before, int count){ }
    	});

        profiles = new ArrayList<ClsProfile>();
        adapter = new AdpContacts(getActivity(),profiles,false);
        list.setAdapter(adapter);
        list.setOnItemClickListener(onContactClick);
        String code = "var friends = API.friends.getRequests({count:100,need_messages:1});" +
                "var profiles = API.getProfiles({uids: friends@.uid,fields: \"photo_medium,photo,online,screen_name,contacts\"});" +
                "return { profiles: profiles};";
        sig = restRequest(code,0);

        return v;
    }
    
    private void saveProfiles (String name) {
    	if (name.trim().equals("")) {
    		savedProfiles = profiles;
    	}
    }
    private void filterByName (String name) {
    	if (name.trim().equals("")) {
    		profiles = savedProfiles;
    		adapter = new AdpContacts(getActivity(),profiles,false);
            list.setAdapter(adapter);
	    	adapter.notifyDataSetChanged();
	    	return;
    	}
    }
    private OnItemClickListener onContactClick = new AdapterView.OnItemClickListener() 
    {
		@Override
		public void onItemClick(AdapterView<?> av, View arg1,
				int position, long arg3) {
			ClsProfile p = profiles.get(position);

			if (currentMode==0) {//req
				ActRequest.name = p.getFirst_name();
				ActRequest.phone = p.getMobile_phone();
				ActRequest.url = p.getPhoto();
				ActRequest.uid = p.getUid();
				ClsDialog d = new ClsDialog();
				d.setUid(p.getUid());
				d.setProfile(p);
				AndroidApplication.setDlg(d);
				startActivity(ActRequest.createIntent(activity));
				
			}
			else {
				ClsDialog d = new ClsDialog();
				d.setUid(p.getUid());
				d.setProfile(p);
				AndroidApplication.setDlg(d);
				startActivity(ActDialog.createIntent(getActivity()));
			}
			
		}
    };
    private void onRESTResult(int code, String result, int mode) {
        Log.d(TAG, " onRESTResult:"+result);
        progressBar.setVisibility(View.GONE);
    	progressBar.clearAnimation();
        if (result != null && result.contains("error_msg")) {
			try {
				JSONObject o = new JSONObject(result);
				o = o.optJSONObject("error");
				Toast.makeText(activity, "Error:"+o.optString("error_msg"), Toast.LENGTH_LONG).show();
			}
			catch (Exception e) {}
			return;
		}
        // Check to see if we got an HTTP 200 code and have some data.
        if (code == 200 && result != null) {
        	
            switch(mode) {
            case 0:
            	parseResult(result,false,mode);//��������
            	break;
            case 1:
            	parseResult(result,false,mode);
            	break;
            }

        }
    }
    
    private String restRequest(String code,int mode) {
    	Log.d(TAG, code);
    	progressBar.setVisibility(View.VISIBLE);
    	progressBar.post(new Runnable() {
    	    @Override
    	    public void run() {
    	        AnimationDrawable frameAnimation =
    	            (AnimationDrawable) progressBar.getBackground();
    	        frameAnimation.start();
    	    }
    	});
		String sig = Utils.md5(metod+"&code="+code+ AndroidApplication.getPrefsString("secret"));
        
        //parseResult(Utils.loadFromCashe(sig),true);
        
        Intent intent = new Intent(activity, RESTService.class);
        intent.setData(Uri.parse(AndroidApplication.HOST+metod+"&sig="+sig));
        Bundle params = new Bundle();
        params.putString("code", code);
        intent.putExtra(RESTService.EXTRA_HTTP_VERB, 0x2);
        intent.putExtra(RESTService.EXTRA_PARAMS, params);
        intent.putExtra(RESTService.EXTRA_MODE, mode);
        intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, mReceiver);
        activity.startService(intent);
        return sig;
	}
    

    
    private OnKeyListener onSchTxt = new View.OnKeyListener() {

		@Override
		public boolean onKey(View v, int keyCode, KeyEvent event) {
			if (event.getAction() == KeyEvent.ACTION_DOWN)
	        {
	            switch (keyCode)
	            {
	                case KeyEvent.KEYCODE_DPAD_CENTER:
	                case KeyEvent.KEYCODE_ENTER:
	                	EditText edt = (EditText)v.findViewById(R.id.sch_searchtxt);
	                	//Log.d(TAG, "SCH:"+edt.getText().toString());
	                	
	                	String code="var profiles = API.users.search({q: \""+edt.getText().toString()+"\",fields: \"photo_medium,photo,online,screen_name,contacts\",count:1000});" +
	                    		"return {profiles: profiles};";
	                    sig = restRequest(code,1);
	                    return true;
	                default:
	                    break;
	            }
	        }
	        return false;

		}
    	
    };

    private void parseResult(String result,boolean firstRun,int mode) {
    	currentMode = mode;
    	profiles = new ArrayList<ClsProfile>();
        
    	if (result==null) return;

    	JSONObject mJSONObject = null;
    	boolean isAll = true;
		ClsProfile p;
    	try
        {
	    	mJSONObject = new JSONObject(result.toString());
	    	mJSONObject = mJSONObject.optJSONObject("response");
	    	JSONArray mProfiles = mJSONObject.optJSONArray("profiles");
	    	
	    	if (mProfiles == null) return;
	    	for (int i=mode;i<mProfiles.length();i++) {
	    		mJSONObject = mProfiles.getJSONObject(i);
	    		//"profiles":[{"uid":61745456,"first_name":"�������","mobile_phone":"","last_name":"�������","home_phone":"","screen_name":"id61745456","photo":"http:\/\/cs9531.userapi.com\/u61745456\/e_916e7897.jpg","online":0},{"uid":150882319,"first_name":"�����","mobile_phone":"790676500**","last_name":"����","home_phone":"671-30**","screen_name
	    		
	    		p = new ClsProfile();
	    		if (mode==0) p.setInd_name(getString(R.string.srh_friendrequests));
	    		else {
	    			if (mJSONObject.optString("first_name").length()>0)
	    				p.setInd_name(mJSONObject.optString("first_name").substring(0, 1));
	    			else p.setInd_name("");
	    		}
		    	p.setFirst_name(mJSONObject.optString("first_name"));
		    	p.setLast_name(mJSONObject.optString("last_name"));
		    	p.setScreen_name(mJSONObject.optString("screen_name"));
		    	p.setPhoto(Uri.decode(AndroidApplication.isLow()?mJSONObject.optString("photo"):mJSONObject.optString("photo_medium")));
		    	p.setOnline(mJSONObject.optInt("online"));
		    	p.setUid(mJSONObject.optInt("uid"));
		    	if (isAll) {
	    			//�������������� ��������
		    		profiles.add(p);
	    		}
	    		else {
	    			if (mJSONObject.optInt("online")==1) {
	    				profiles.add(p);
	    			}
	    			
	    		}
	    	}
        }
    	catch (Exception e)
        {
    		Log.e(TAG, e.toString());
        }
    	finally {
    		mJSONObject = null;
    	}
    	Log.d(TAG, "size"+profiles.size());
    	Collections.sort(profiles, new Comparator<ClsProfile>() {
            @Override
            public int compare(ClsProfile p1, ClsProfile p2) {
            	String s1 = p1.getInd_name();
            	String s2 = p2.getInd_name();
                return s1.compareToIgnoreCase(s2);
            }
        });

    	adapter = new AdpContacts(getActivity(),profiles,false);
        list.setAdapter(adapter);
    	adapter.notifyDataSetChanged();
    	if (!firstRun) {
    		//�������� ���
    		//Utils.save2cache(result,sig);
    	}
    }
    @Override
    public void onResume() {
        super.onResume();

    }
}
