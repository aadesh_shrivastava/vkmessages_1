package ikillingtime.com.ivkontaktepro;

import java.util.ArrayList;

public class ClsVideos {
	
	private String id;
	private String vid;
	private String title;
	private String description;
	private String date;
	private String thumb;
	private String image_medium;
	private String player;
	private String duration;
	private boolean menu;
    private String href;
    private boolean viewved;
    private String vidtype;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getThumb() {
		return thumb;
	}
	public void setThumb(String thumb) {
		this.thumb = thumb;
	}
	public String getImage_medium() {
		return image_medium;
	}
	public void setImage_medium(String image_medium) {
		this.image_medium = image_medium;
	}
	public String getPlayer() {
		return player;
	}
	public void setPlayer(String player) {
		this.player = player;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public boolean isMenu() {
		return menu;
	}
	public void setMenu(boolean menu) {
		this.menu = menu;
	}
	public String getVid() {
		return vid;
	}
	public void setVid(String vid) {
		this.vid = vid;
	}

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public boolean isViewved() {
        return viewved;
    }

    public void setViewved(boolean viewved) {
        this.viewved = viewved;
    }
    public String getVidtype() {
        return vidtype;
    }
    public void setVidtype(String vidtype) {
        this.vidtype = vidtype;
    }
}
