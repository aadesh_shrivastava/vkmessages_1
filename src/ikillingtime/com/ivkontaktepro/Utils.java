package ikillingtime.com.ivkontaktepro;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import ikillingtime.com.ivkontaktepro.R;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.util.Log;

public class Utils {
	private static final String TAG = "Utils";
	
	public static final String md5(final String s) {
		//return new String(DigestUtils.md5Hex(s));

	    try {
	        // Create MD5 Hash
	        MessageDigest digest = java.security.MessageDigest
	                .getInstance("MD5");
	        digest.reset();
			digest.update(s.getBytes("UTF-8"));

	        byte messageDigest[] = digest.digest();

	        // Create Hex String
	        StringBuffer hexString = new StringBuffer();
	        for (int i = 0; i < messageDigest.length; i++) {
	            String h = Integer.toHexString(0xFF & messageDigest[i]);
	            while (h.length() < 2)
	                h = "0" + h;
	            hexString.append(h);
	        }
	        return hexString.toString();

	    } catch (NoSuchAlgorithmException e) {
	        e.printStackTrace();
	    }
	    catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	    return "";
	    
	}
	
	public static String loadFromCashe(String sig) {
    	
		if (AndroidApplication.getAppCache()==null) 
			return null;
		BufferedReader reader = null;
		StringBuilder builder = new StringBuilder();
		try {
			File f = new File(AndroidApplication.getAppCache(), sig);
			reader = new BufferedReader(new FileReader(f.getAbsolutePath()));
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG,"loadFromCashe");
			return null;
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return builder.toString();
	}
	
	public static void save2cache(String result,String sig) {
    	
    	if (AndroidApplication.getAppCache()!=null) {
			try {
				File f = new File(AndroidApplication.getAppCache(), sig);
				BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(f));
				os.write(result.getBytes());
				os.flush();
				os.close();
     
			}
			catch (Exception e) {
				Log.e(TAG,"save2cache"+sig);
			}
		}
    }
	
	public static String convertTime(Date date) {
    	StringBuilder s = new StringBuilder();
    	Calendar cal = Calendar.getInstance();
		
		int currentDay = cal.get(Calendar.DAY_OF_MONTH);
		int currentMonth = cal.get(Calendar.MONTH);
		int currentYear = cal.get(Calendar.YEAR); 
		
		cal.set(date.getYear()+1900, date.getMonth()+1, date.getDate());			
		
    	if (currentYear!=(date.getYear()+1900) || currentMonth!=date.getMonth() || currentDay!=date.getDate() ) {
    		if (date.getDate()<10) s.append("0");
    		s.append(""+date.getDate());
    		s.append(".");
    		if (date.getMonth()+1<10) s.append("0");
    		s.append(""+(date.getMonth()+1));
    		if (date.getYear()+1900!=currentYear) {
    			s.append(".");
    			//������� � ������ ��� ��������� �� 1999 ���� ����....
    			s.append(""+(date.getYear()+1900));//-2000));
    			// �� �������� �����!
    		}
    		
    	}
    	else {
    		if (date.getHours()<10) s.append("0");
    		s.append(""+date.getHours());
    		s.append(":");
    		if (date.getMinutes()<10) s.append("0");
    		s.append(""+date.getMinutes());
    	}
    	return s.toString();
    }
	
	public static File createImageFile() throws IOException {
        String imageFileName =  "temp_upload";
        File image = File.createTempFile(
            imageFileName, 
            ".JPG", 
            AndroidApplication.getAppCache()
        );
        return image;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
	
	public static boolean compressBmp(String file) {
		//������ �������� ����� ������ ����� � ��������, ������� � ���� ����� ���� ����
		OutputStream out = null;
		
		try {
			int targetW = 800;
	        int targetH = 600;
	      
	        // Get the dimensions of the bitmap
	        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
	        bmOptions.inJustDecodeBounds = true;
	        BitmapFactory.decodeFile(file, bmOptions);
	        int photoW = bmOptions.outWidth;
	        int photoH = bmOptions.outHeight;
	      
	        // Determine how much to scale down the image
	        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);
	      
	        // Decode the image file into a Bitmap sized to fill the View
	        bmOptions.inJustDecodeBounds = false;
	        bmOptions.inSampleSize = scaleFactor;
	        bmOptions.inPurgeable = true;
	      
	        Bitmap bitmap = BitmapFactory.decodeFile(file, bmOptions);
			
		
			out = new BufferedOutputStream(new FileOutputStream(file));
		    bitmap.compress(CompressFormat.JPEG, 75, out);
		} catch (Exception e) {
			return false;
		}
		finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					return false;
				}
		    }
		}
		return true;
	}
}
