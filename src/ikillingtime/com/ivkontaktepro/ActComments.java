package ikillingtime.com.ivkontaktepro;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * User: recoilme
 * Date: 26.06.13
 * Time: 21:41
 * To change this template use File | Settings | File Templates.
 */
public class ActComments extends ListActivity {
    private static final String TAG="ActComments";
    private AQuery aq;
    private ProgressBar progress;
    private Activity activity;
    private int offset=0;
    private String postId="",ownerId="";
    private AdpComments adapter;
    private ArrayList<JSONObject> items;
    private HashMap<String,ClsUser> groupsMap;
    private boolean isRunning=false;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comments);

        activity = this;
        //AQUtility.debug(TAG,AndroidApplication.getPrefsString("post"));
        //{"post_source":{"type":"vk"},"text":"#Арт_Дня<br>#by_Akiba","signer_id":48247452,"reposts":{"count":4,"user_reposted":0},"source_id":-8285618,"likes":{"can_publish":1,"can_like":1,"user_likes":0,"count":65},"post_type":"post","attachments":[{"type":"photo","photo":{"text":"","width":571,"pid":305823148,"aid":-7,"post_id":73844,"src_xbig":"http:\/\/cs14110.vk.me\/c540102\/v540102452\/69e7\/E6Q3hSh9JX8.jpg","height":800,"src_small":"http:\/\/cs14110.vk.me\/c540102\/v540102452\/69e4\/Ycyt1UJ4oHk.jpg","created":1372259151,"owner_id":-8285618,"user_id":100,"access_key":"5d3309a6cc89948f91","src":"http:\/\/cs14110.vk.me\/c540102\/v540102452\/69e5\/DYQFGBzz5Dw.jpg","src_big":"http:\/\/cs14110.vk.me\/c540102\/v540102452\/69e6\/D_olKWpOpXw.jpg"}}],"attachment":{"type":"photo","photo":{"text":"","width":571,"pid":305823148,"aid":-7,"post_id":73844,"src_xbig":"http:\/\/cs14110.vk.me\/c540102\/v540102452\/69e7\/E6Q3hSh9JX8.jpg","height":800,"src_small":"http:\/\/cs14110.vk.me\/c540102\/v540102452\/69e4\/Ycyt1UJ4oHk.jpg","created":1372259151,"owner_id":-8285618,"user_id":100,"access_key":"5d3309a6cc89948f91","src":"http:\/\/cs14110.vk.me\/c540102\/v540102452\/69e5\/DYQFGBzz5Dw.jpg","src_big":"http:\/\/cs14110.vk.me\/c540102\/v540102452\/69e6\/D_olKWpOpXw.jpg"}},"post_id":73844,"date":1372327209,"type":"post","comments":{"count":1,"can_post":1}}
        progress = (ProgressBar) findViewById(R.id.progressBar);
        ListView list = getListView();
        
        try {
            JSONObject jsonObject = new JSONObject(AndroidApplication.getPrefsString("post", ""));
            postId = jsonObject.optString("post_id");
            ownerId = jsonObject.optString("source_id");
        }
        catch(Exception e) {
            AQUtility.debug(e);
        }
        if (postId.equals("") | ownerId.equals("")){
            finish();
        }
        aq = new AQuery(activity);
        items = new ArrayList<JSONObject>();
        groupsMap = new HashMap<String,ClsUser>();
        adapter = new AdpComments(activity,items,groupsMap);
        list.setAdapter(adapter);
        list.setOnScrollListener(onScroll);
        request();
    }

    private AbsListView.OnScrollListener onScroll = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view,
                                         int scrollState) {
        }
        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (visibleItemCount > 0 && firstVisibleItem + visibleItemCount == totalItemCount && !isRunning ) {
                request();
            }
        }
    };
    
    public void request(){
        isRunning = true;
        String code = "var comm = API.wall.getComments({count:100,post_id:"+postId+",owner_id:"+ownerId+",offset:"+offset+"});" +
                "var profiles = API.getProfiles({uids: comm@.uid,fields: \"photo_medium,online,screen_name\"});" +
                "return {comm: comm, profiles: profiles};";
        SortedMap<String, String> paramsMap = new TreeMap<String, String>();
        paramsMap.put("code", code);

        aq.progress(progress).ajax(generateUri4Post(paramsMap), paramsMap, JSONObject.class, activity, "renderComments");
    }

    public static String generateUri4Post(SortedMap<String, String> paramsMap) {
        StringBuilder paramsString = new StringBuilder();
        for (String key : paramsMap.keySet()) {
            paramsString.append("&");
            paramsString.append(key);
            paramsString.append("=");
            paramsString.append(paramsMap.get(key).toString());
        }

        //AQUtility.debug(TAG,ActNewsOld.generateUrl(params, ""));
        String methodString = "/method/execute?access_token="+AndroidApplication.getPrefsString("access_token");
        String sig = Utils.md5(methodString+paramsString.toString()+ AndroidApplication.getPrefsString("secret"));
        String url = AndroidApplication.HOST+methodString+"&sig="+sig;
        return url;
    }

    public void renderComments(String url, JSONObject json, AjaxStatus status){
        AQUtility.debug(TAG,json);
        //ActComments:{"response":{"comm":[6,{"uid":90248316,"text":"Откуда скрин?","date":1372341647,"from_id":90248316,"cid":73853},{"uid":26546448,"reply_to_uid":90248316,"text":"[id90248316|Сенбек], старшая школа демонов","date":1372341688,"reply_to_cid":73853,"from_id":26546448,"cid":73854},{"uid":79252074,"text":"что за аниме на гифке? http:\/\/vk.com\/doc79252074_198433261","date":1372342370,"from_id":79252074,"cid":73855},{"uid":19108343,"reply_to_uid":79252074,"text":"[id79252074|Сергей], школа демонов ОВА","date":1372342419,"reply_to_cid":73855,"from_id":19108343,"cid":73856},{"uid":181197311,"reply_to_uid":79252074,"text":"[id79252074|Сергей], Мы ждём тебя летом","date":1372342462,"reply_to_cid":73855,"from_id":181197311,"cid":73857},{"uid":49145312,"text":"Ох, дочего ужасная и трешовая ова... Впрочем, как и все овы по сериалам ТВ формата.","date":1372342638,"from_id":49145312,"cid":73858}],
        // "profiles":[{"uid":90248316,"first_name":"Сенбек","last_name":"Каратаев","screen_name":"id90248316","photo_medium":"http:\/\/cs10791.vk.me\/u90248316\/d_8fa22d4a.jpg","photo":"http:\/\/cs10791.vk.me\/u90248316\/e_bea22457.jpg","online_mobile":1,"online_app":"3140623","online":1},{"last_name":"Бараусов","uid":26546448,"first_name":"Андрей","screen_name":"id26546448","photo_medium":"http:\/\/cs308724.vk.me\/v308724448\/5a4c\/--Sn7mCAnxE.jpg","photo":"http:\/\/cs308724.vk.me\/v308724448\/5a4d\/4_xUXS42_Jk.jpg","online":1},{"last_name":"Потёмкин","uid":79252074,"first_name":"Сергей","screen_name":"s.potyomkin","photo_medium":"http:\/\/cs408822.vk.me\/v408822074\/203e\/XYjoUe058Bs.jpg","photo":"http:\/\/cs408822.vk.me\/v408822074\/203f\/1dbJ3bPc77Y.jpg","online":1},{"last_name":"Генкши","uid":19108343,"first_name":"Владислав","screen_name":"genkshi","photo_medium":"http:\/\/cs309116.vk.me\/v309116343\/498a\/XIZlBOMme2k.jpg","photo":"http:\/\/cs309116.vk.me\/v309116343\/498b\/lVCYT559HxY.jpg","online":1},{"last_name":"Spiridonov","uid":181197311,"first_name":"Igor","screen_name":"candidgerm","photo_medium":"http:\/\/cs302102.vk.me\/v302102311\/60be\/_8lKFcwWC3c.jpg","photo":"http:\/\/cs302102.vk.me\/v302102311\/60bf\/bPq80ruPMm8.jpg","online":1},{"uid":49145312,"first_name":"Данил","last_name":"Глухов","screen_name":"dancel94","photo_medium":"http:\/\/cs405529.vk.me\/v405529312\/cec1\/T-fA24wmZGI.jpg","photo":"http:\/\/cs405529.vk.me\/v405529312\/cec2\/MPp1qYQQNrw.jpg","online_mobile":1,"online_app":"2274003","online":1}]}}
        int total = 0;
        if (status.getCode()==200){
            adapter.notifyDataSetInvalidated();
            //new_offset = o.optString("new_offset");
            if (offset==0) {
                items.clear();
            }
            try {
                JSONObject response = json.getJSONObject("response");
                JSONArray comm = response.getJSONArray("comm");
                total = comm.optInt(0);
                for (int i=1;i<comm.length();i++){
                    JSONObject jsonObject = comm.getJSONObject(i);
                    items.add(comm.getJSONObject(i));
                }
                JSONArray profiles = response.optJSONArray("profiles");
                if (profiles!=null) {
                    for(int j=0;j<profiles.length();j++) {
                        JSONObject p = profiles.optJSONObject(j);
                        groupsMap.put(p.optString("uid"),new ClsUser(p.optString("uid"),p.optString("first_name")+" "+p.optString("last_name"),p.optString("photo_medium")));
                    }
                }
            }
            catch (Exception e) {
                AQUtility.debug(TAG,e);
            }
            if (total>=100) {
                offset+=100;
                isRunning = false;
            }
            adapter.notifyDataSetChanged();
        }
        else {
            AQUtility.debug(TAG,status.getError());
        }

    }

    public static Intent createIntent(Context context) {
        Intent i = new Intent(context, ActComments.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return i;
    }

    public void onBack(View v) {
        finish();
    }


}