package ikillingtime.com.ivkontaktepro;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import com.androidquery.util.AQUtility;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.imageloader.AsyncImageView;

public class ActCaptcha extends Activity {
	private static final String TAG = "ActCaptcha";
	public static final String EXTRA_CAPTCHA_PARAMS          = "EXTRA_CAPTCHA_PARAMS";
	private AsyncImageView img;
	private EditText txt;
	private Bundle params;
	private String captcha_sid;
    public static Bundle result;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.captcha);
        result = null;
        img = (AsyncImageView) findViewById(R.id.captcha_thumb);
        txt = (EditText) findViewById(R.id.captcha_txt);
        Bundle extras = getIntent().getExtras();
        params = new Bundle();
        captcha_sid = "";
        parseCaptcha(extras.getString("result"));
    }

	private void parseCaptcha(String result) {
		Log.d(TAG, "parseCaptcha");
		//{"error":{"error_code":14,"error_msg":"Captcha needed","request_params":[{"key":"oauth","value":"1"},
		//{"key":"method","value":"messages.send"},{"key":"access_token","value":"648f28336721010367210103e0670c3f32767216728110142eeeead5d0f0ecd"},
		//{"key":"sig","value":"05b9fe7b0cbaa61d161b8158384f404d"},
		//{"key":"message","value":"Norm"},{"key":"uid","value":"61745456"}],"captcha_sid":"852070398686","captcha_img":"http:\/\/api.vk.com\/captcha.php?sid=852070398686&s=1"}}

        // {"error":"need_captcha","captcha_sid":"228766473492","captcha_img":"http:\/\/api.vk.com\/captcha.php?sid=228766473492"}null:https://api.vk.com/oauth/token?grant_type=password&client_id=296

        JSONObject mJSONObject = null;
		String captcha_img="";
        AQUtility.debug("captcha_str",result);
		try {
			mJSONObject = new JSONObject(result.toString());

	    	mJSONObject = mJSONObject.optJSONObject("error");
            if (mJSONObject!=null) {

                JSONArray mParams = mJSONObject.optJSONArray("request_params");
               String key = "";

                for (int i=0;i<mParams.length();i++) {
                    JSONObject  obj = mParams.getJSONObject(i);
                    key = obj.optString("key");
                    if (key.equals("oauth") || key.equals("access_token") || key.equals("sig"))
                        continue;
                    params.putString(key, obj.optString("value"));
                    //Log.d(TAG, mParams.get(i).toString());
                }
            }
            else {
                mJSONObject = new JSONObject(result.toString());
            }

	    	captcha_sid = mJSONObject.optString("captcha_sid");
	    	params.putString("captcha_sid", captcha_sid);
	    	captcha_img = Uri.decode(mJSONObject.optString("captcha_img"));
            AQUtility.debug("captcha_img",captcha_img);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
    		mJSONObject = null;
    	}
		if (!captcha_sid.equals("") && !captcha_img.equals("")) {
			img.setUrl(captcha_img);
			
		}
	}
	
	public void onCheck(View v) {
		String s = txt.getText().toString();
		if (s.equals("")) {
			Animation shake = AnimationUtils.loadAnimation(ActCaptcha.this, R.anim.shake);
			txt.startAnimation(shake);
			return;
		}
		Intent intent= new Intent(this, ActCaptcha.class);
		params.putString("captcha_key", s);
    	intent.putExtra(EXTRA_CAPTCHA_PARAMS, params);
        result = params;
    	setResult(RESULT_OK, intent);
    	finish();
	}
}