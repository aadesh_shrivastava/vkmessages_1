# iVk

Free android client for vk.com.

**ivk can:**

- news, read & search, comments. Group by public
- videos, video of groups, search, mark watched
- messages, chats, photos, voice input
- music, listen, search, music from device, shake for next track
- friends, search, online
- settings

**Contribute:**

It is an open source and you're welcome to contribute.

**Apk file:**

[Latest stable apk file](https://bitbucket.org/recoilme/vkmessages/downloads/ivk.apk)

**Get it on Google Play:**

Deleted by google

**Screenshots:**

![Screenshot](https://bitbucket.org/recoilme/vkmessages/raw/master/screen1.png)![Screenshot](https://bitbucket.org/recoilme/vkmessages/raw/master/screen2.png)

### Author
Kulibaba Vadim <<vadim.kulibaba@gmail.com>>

### License
Distributed under [Apache 2 license](https://bitbucket.org/recoilme/freeamp/raw/master/LICENSE.txt).
